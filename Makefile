# You may want to use 'gmake' on BSD systems.

MYKEY := BA8B7396

CC := $(shell which c++ 2>/dev/null || echo cpp)
EXE = linapple
PROF = -O -g3
#PROF = -O

C_FLAGS = $(PROF) -Wall $(shell sdl-config --cflags) $(shell curl-config --cflags)
#C_FLAGS = -c -Wall -Wextra -pedantic -std=c11 -Iinclude
#L_FLAGS = $(PROF) -lcrypt
L_FLAGS = $(PROF) $(shell sdl-config --libs) $(shell curl-config --libs) -lz -lzip

# supress gnupg usage if not present
GPG := $(shell gpg --list-secret-keys $(MYKEY) 1>/dev/null 2>&1 && echo "-u $(MYKEY)")

BLD_KERNEL   := $(shell uname -s)
BLD_KERN_REL := $(shell uname -r)
BLD_HOSTNAME := $(shell hostname)
BLD_TIME := $(shell LC_ALL=C date)
VERSION  := $(shell cat version)
RELEASE  := $(shell cat release)
REVISION := $(shell git rev-parse --short=8 HEAD 2>/dev/null \
				|| echo no-git-revision)
CWD := $(shell pwd)

.PHONY: clean


# this scary thing returns random word from dictionary for release name
ROLLDICE := $(shell perl -e 'open F,"</usr/share/dict/american-english"or exit 1;@l=<F>;do{$$r=rand scalar @l}until($$l[$$r]=~s/([A-Z][a-z]*).*/$$1/&&$$l[$$r]=~y===c>8);print $$l[$$r]' || echo no-dices)

#C_FLAGS += -DREVISION="\"$(REVISION)\""
#C_FLAGS += -DRELEASE="\"$(RELEASE)\""
#C_FLAGS += -DVERSION="\"$(VERSION)\""

# Source Files
SRC_FILES := $(filter-out src/Util_MemoryTextFile.cpp,$(wildcard src/*.cpp))

# Object Files
OBJ_DIR = obj
OBJ_FILES := $(patsubst src/%.cpp, $(OBJ_DIR)/%.o, $(SRC_FILES))
JSON_PARTS := $(patsubst src/%.cpp, $(OBJ_DIR)/%.jpart, $(SRC_FILES))

# Make the Object directory if it doesn't exist
$(shell   mkdir -p $(OBJ_DIR))

build: version_info $(EXE)

# the idea is to rebuild only one file each time you run make
# to have the actual /project/ version info available through
# function calls in version.o
version_info: src/version.h
	rm -f obj/version.o

compile_commands.json: $(JSON_PARTS)
	@echo "[" >compile_commands.json
	@cat $(OBJ_DIR)/*.jpart >>compile_commands.json
	@echo "]" >>compile_commands.json
	@rm -f $(OBJ_DIR)/*.jpart

$(EXE): $(OBJ_FILES) bin
	rm -f $(EXE)
	$(CC) -o $(EXE) $(OBJ_FILES) $(L_FLAGS)

$(OBJ_DIR)/%.jpart: src/%.cpp
	@echo "  { \"directory\": \"$(CWD)\"," >$@
	@echo "    \"command\": \"$(CC) $(C_FLAGS) -c $<\"," >>$@
	@echo "    \"file\": \"$<\" }," >>$@

$(OBJ_DIR)/%.o: src/%.cpp
	$(CC) $(C_FLAGS) -c -o $@ $<

bin:
	mkdir -p bin

#####################
## simple release management, src-based
##
## make release -- to increment micro version (*.*.X+1)
## make minor   -- to increment minor version (*.X+1.*)
## make major   -- to increment major version (X+1.*.*)
##

# make changes to the maintainer's files and then commit it
#newtag: askfirst debian/install debian/changelog include/$(PACKAGE)_version.hrl
newtag: askfirst CHANGELOG
	echo "$(NEWVER)" > version
	echo "$(ROLLDICE)" > release
	git commit -am "release: $(NEWVER) $(ROLLDICE)"
	git tag $(GPG) -m"New release: $(ROLLDICE)" $(NEWVER) HEAD

askfirst:
	@echo "$(VERSION) -> $(NEWVER), codename: $(ROLLDICE)"
	@echo "are you sure?"
	@read yn; if [ "_$$yn" != "_y" ]; then echo "abort."; exit 2; fi
	@echo "ok, proceeding"

.PHONY: CHANGELOG
CHANGELOG:
	mv CHANGELOG CHANGELOG.prev
	echo "Release: $(NEWVER) $(ROLLDICE)" >>CHANGELOG
	echo "$BLD_TIME" >>CHANGELOG
	git log --oneline $(VERSION).. >>CHANGELOG
	echo "---" >>CHANGELOG
	echo "" >>CHANGELOG
	cat CHANGELOG.prev >>CHANGELOG
	rm CHANGELOG.prev

.PHONY: src/version.h
src/version.h: src/version.h.in
	sed "s/{{VERSION}}/$(VERSION)/g; \
		 s/{{RELEASE}}/$(RELEASE)/g; \
		 s/{{BLD_TIME}}/$(BLD_TIME)/g; \
		 s/{{BLD_HOSTNAME}}/$(BLD_HOSTNAME)/g; \
		 s/{{BLD_KERNEL}}/$(BLD_KERNEL)/g; \
		 s/{{BLD_KERN_REL}}/$(BLD_KERN_REL)/g" $< > $@

# increment micro version number
release: NEWVER=$(shell perl -e '@a=split /\./,"$(VERSION)";print "$$a[0].$$a[1].".($$a[2]+1)')
release: newtag push

# the same as release, but changing minor version digit
minor: NEWVER = $(shell perl -e '@a=split /\./,"$(VERSION)";print "$$a[0].".($$a[1]+1).".0"')
minor: newtag push

# the same as release, but change major version digit
major: NEWVER = $(shell echo $(VERSION)| perl -e '@a=split /\./,<>;print(($$a[0]+1).".0.0")')
major: newtag push

# push commits and tags to the upstream server
.PHONY: push
push:
	git push
	git push --tags


###################
## cleanup as usual

clean:
	$(RM) -f $(OBJ_FILES) $(EXE) *~ *.bak *.orig *.rej compile_commands.json

distro:
	make clean
	cd ../..
	$(RM) -f log/*.log
	tar zvcf quickmud-`date -I`.tar.gz QuickMUD
